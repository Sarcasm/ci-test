# Introduction

Experimenting with Gitlab, Conda and CMake.

# Build

    conda env create -f environment.yaml
    conda env update -f environment.yaml
    conda activate hello-dev
    mkdir build
    cd build
    cmake -G Ninja -DCMAKE_CXX_FLAGS='-fdiagnostics-color -Wall -Wextra' ..
    ninja
