#include "hello/hello.h"

#include <fmt/core.h>

namespace hello {

void hello() { fmt::print("Hello!\n"); }

} // namespace hello
